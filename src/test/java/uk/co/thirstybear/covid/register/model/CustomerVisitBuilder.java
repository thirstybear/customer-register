package uk.co.thirstybear.covid.register.model;

import java.time.LocalDateTime;

public class CustomerVisitBuilder {
  private String name;
  private String phone;
  private int groupCount;
  private LocalDateTime date;

  public static CustomerVisitBuilder aCustomerVisit() {
    return new CustomerVisitBuilder();
  }

  public CustomerVisitBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public CustomerVisitBuilder withPhone(String phone) {
    this.phone = phone;
    return this;
  }

  public CustomerVisitBuilder withGroupCount(int groupCount) {
    this.groupCount = groupCount;
    return this;
  }

  public CustomerVisitBuilder withDate(LocalDateTime date) {
    this.date = date;
    return this;
  }

  public CustomerVisit build() {
    return new CustomerVisit(name, phone, groupCount, date);
  }
}