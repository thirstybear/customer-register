package uk.co.thirstybear.covid.register.security;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ClientIPTest {
  @Test
  public void returnsXForwardForFieldIfPresent() {
    HttpRequestData mockHttpRequestData = mock(HttpRequestData.class);
    when(mockHttpRequestData.getXForwardFor()).thenReturn(Optional.of("192.168.1.2"));

    assertThat(new ClientIP(mockHttpRequestData).toString(), is("192.168.1.2"));
  }

  @Test
  public void returnsRequestRemoteAddressIfNoXFFHeader() {
    HttpRequestData mockHttpRequestData = mock(HttpRequestData.class);
    when(mockHttpRequestData.getXForwardFor()).thenReturn(Optional.empty());
    when(mockHttpRequestData.getRemoteAddr()).thenReturn("192.168.3.4");

    assertThat(new ClientIP(mockHttpRequestData).toString(), is("192.168.3.4"));
  }


}