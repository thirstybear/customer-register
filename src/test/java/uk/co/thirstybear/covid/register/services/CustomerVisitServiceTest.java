package uk.co.thirstybear.covid.register.services;

import org.junit.jupiter.api.Test;
import uk.co.thirstybear.covid.register.db.CustomerVisitRepository;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class CustomerVisitServiceTest {
    @Test
    public void deletesFieldsOlderThan21Days() {
      LocalDateTime currentDateTime = LocalDateTime.of(2020, Month.JULY, 31, 17, 20);
      Clock fixedClock = Clock.fixed(currentDateTime.toInstant(UTC),
                                  ZoneId.of("UTC"));

      CustomerVisitRepository mockCustomerVisitRepo = mock(CustomerVisitRepository.class);
      CustomerVisitService customerVisitService = new CustomerVisitService(mockCustomerVisitRepo, fixedClock);

      customerVisitService.expireOldRecords();

      LocalDateTime expectedDate = LocalDateTime.of(2020, Month.JULY, 10, 17, 20);
      verify(mockCustomerVisitRepo).deleteByDateBefore(expectedDate);
    }


}