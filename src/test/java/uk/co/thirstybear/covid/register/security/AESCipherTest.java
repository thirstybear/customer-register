package uk.co.thirstybear.covid.register.security;

import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import uk.co.thirstybear.covid.register.db.AESCipher;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AESCipherTest {
  @Test
  public void encryptionIsReversible() {
    String plaintext = "my random text";

    AESCipher AESCipher = new AESCipher("passphrase");
    byte[] encryptedText = AESCipher.encrypt(plaintext);

    assertThat(encryptedText, is(not(plaintext.getBytes())));
    assertThat(AESCipher.decrypt(encryptedText), is(plaintext));
  }

  @Test
  public void encryptionIsNotInstanceDependent() {
    String plaintext = "my random text";

    AESCipher AESCipher = new AESCipher("passphrase");
    byte[] encryptedText = AESCipher.encrypt(plaintext);

    assertThat(new AESCipher("passphrase").decrypt(encryptedText), is(plaintext));
  }

  @Test
  public void encryptsNullAsEmptyString() {
    AESCipher AESCipher = new AESCipher("passphrase");
    byte[] encryptedText = AESCipher.encrypt(null);
    assertThat(new AESCipher("passphrase").decrypt(encryptedText), is(""));
  }

  @Test
  public void failsIfDecryptValueIsNotLongEnough() {
    AESCipher AESCipher = new AESCipher("passphrase");

    assertThrows(AESCipher.EncryptionException.class, () -> AESCipher.decrypt(new byte[32]));
  }

    @Test
    public void runsFastEnough() {
      String passphrase = RandomString.make(14);
      String plainText = RandomString.make(100);

      AESCipher cipher = new AESCipher(passphrase);
      byte[] encryptedText = cipher.encrypt(plainText);

      long start = System.currentTimeMillis();
        new AESCipher(passphrase).decrypt(encryptedText);
      long end = System.currentTimeMillis();

      long elapsed = end - start;
      System.out.println("Elapsed " + elapsed);

      assertThat(elapsed, lessThan(50L));
    }

}