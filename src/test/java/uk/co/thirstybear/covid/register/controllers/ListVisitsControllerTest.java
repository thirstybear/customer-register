package uk.co.thirstybear.covid.register.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import uk.co.thirstybear.covid.register.model.CustomerVisit;
import uk.co.thirstybear.covid.register.services.CustomerVisitService;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static uk.co.thirstybear.covid.register.model.CustomerVisitBuilder.aCustomerVisit;

class ListVisitsControllerTest {
  @Test
  public void returnsListOfVisitors() {
    CustomerVisit visit1 = aCustomerVisit()
        .withName("name1")
        .withPhone("phone1")
        .withGroupCount(1)
        .withDate(LocalDateTime.of(2020, 6, 1, 11, 0))
        .build();
    CustomerVisit visit2 = aCustomerVisit()
        .withName("name2")
        .withPhone("phone2")
        .withGroupCount(2)
        .withDate(LocalDateTime.of(2020, 7, 1, 11, 0))
        .build();
    CustomerVisit visit3 = aCustomerVisit()
        .withName("name3")
        .withPhone("phone3")
        .withGroupCount(3)
        .withDate(LocalDateTime.of(2020, 8, 1, 11, 0))
        .build();

    List<CustomerVisit> listOfVisits = List.of(visit1, visit2, visit3);

    CustomerVisitService mockCustomerVisitService = mock(CustomerVisitService.class);
    when(mockCustomerVisitService.allVisits()).thenReturn(listOfVisits);

    Model model = new ExtendedModelMap();
    assertThat(new ListVisitsController(mockCustomerVisitService).listOfRegisteredVisitors(model), is("listview"));

    //noinspection unchecked
    assertThat((List<CustomerVisit>) model.getAttribute("visits"), containsInAnyOrder(visit1, visit2, visit3));
    assertThat(model.getAttribute("registrations"), is(3));
    assertThat(model.getAttribute("visitors"), is(6));
  }

}