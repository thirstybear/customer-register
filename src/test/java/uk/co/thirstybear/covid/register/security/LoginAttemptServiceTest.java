package uk.co.thirstybear.covid.register.security;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class LoginAttemptServiceTest {
  @Test
  public void failedLoginsAreBlockedAfterMax() {
    String mykey = "192.168.1.1";
    LoginAttemptService loginAttemptService = new LoginAttemptService();

    for (int i = 1; i <= LoginAttemptService.MAX_ATTEMPTS; i++) {
      loginAttemptService.loginFailed(mykey);
      assertThat(loginAttemptService.isBlocked(mykey), is(false));
    }

    loginAttemptService.loginFailed(mykey);
    assertThat(loginAttemptService.isBlocked(mykey), is(true));
  }

    @Test
    public void loggingInABlockedAccountResetsIt() {
      String mykey = "192.168.1.1";
      LoginAttemptService loginAttemptService = new LoginAttemptService();

      for (int i = 0; i <= LoginAttemptService.MAX_ATTEMPTS; i++) {
        loginAttemptService.loginFailed(mykey);
      }
      assertThat(loginAttemptService.isBlocked(mykey), is(true));

      loginAttemptService.loginSucceeded(mykey);
      assertThat(loginAttemptService.isBlocked(mykey), is(false));

    }


}