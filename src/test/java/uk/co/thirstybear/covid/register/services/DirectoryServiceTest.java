package uk.co.thirstybear.covid.register.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static uk.co.thirstybear.covid.register.services.DirectoryService.BACKUPFILE_REGEX;

class DirectoryServiceTest {

  private Path tempDirectory;

  @AfterEach
  public void deleteTempDirectory() throws IOException {
    Files.list(tempDirectory).forEach(FilesWrapper::delete);
    Files.delete(tempDirectory);
  }

  @BeforeEach
  void setUp() throws IOException {
    tempDirectory = Files.createTempDirectory("testing-");
  }

  @Test
  public void keepsLast5Backups() throws IOException {
    Files.createFile(tempDirectory.resolve("db-200701-2101.zip"));
    Files.createFile(tempDirectory.resolve("db-200702-2213.zip"));
    Files.createFile(tempDirectory.resolve("db-200703-0600.zip"));
    Files.createFile(tempDirectory.resolve("db-200704-0700.zip"));
    Files.createFile(tempDirectory.resolve("db-200705-2333.zip"));
    Files.createFile(tempDirectory.resolve("db-200628-0821.zip"));
    Files.createFile(tempDirectory.resolve("db-200630-0742.zip"));
    Files.createFile(tempDirectory.resolve("db-200629-0534.zip"));

    new DirectoryService(tempDirectory).cleanBackups();

    List<String> files = Files.list(tempDirectory)
        .map(file -> file.getFileName().toString())
        .collect(Collectors.toList());
    assertThat(files.size(), is(5));
    assertThat(files,
        containsInAnyOrder("db-200701-2101.zip", "db-200702-2213.zip", "db-200703-0600.zip",
            "db-200704-0700.zip", "db-200705-2333.zip"));
  }

  @Test
  public void doesNotDeleteAnythingIfLessThan6Files() throws IOException {
    Files.createFile(tempDirectory.resolve("db-200701-2101.zip"));
    Files.createFile(tempDirectory.resolve("db-200702-2213.zip"));
    Files.createFile(tempDirectory.resolve("db-200703-0600.zip"));

    new DirectoryService(tempDirectory).cleanBackups();

    List<String> files = Files.list(tempDirectory)
        .map(file -> file.getFileName().toString())
        .collect(Collectors.toList());
    assertThat(files.size(), is(3));
    assertThat(files,
        containsInAnyOrder("db-200701-2101.zip", "db-200702-2213.zip", "db-200703-0600.zip"));
  }

  @Test
  public void ignoresNonBackupFiles() throws IOException {
    Files.createFile(tempDirectory.resolve("db-200701-2100.zip"));
    Files.createFile(tempDirectory.resolve("db-200702.zip"));
    Files.createFile(tempDirectory.resolve("db-200703-1316.zip"));
    Files.createFile(tempDirectory.resolve("db-20070-1414.zip"));
    Files.createFile(tempDirectory.resolve("db-200705.zip"));
    Files.createFile(tempDirectory.resolve("200628-15.zip"));
    Files.createFile(tempDirectory.resolve("db-200630.txt"));
    Files.createFile(tempDirectory.resolve("randomname.db"));

    new DirectoryService(tempDirectory).cleanBackups();
    List<String> files = Files.list(tempDirectory)
        .map(file -> file.getFileName().toString())
        .collect(Collectors.toList());
    assertThat(files.size(), is(8));
  }

  @Test
  public void generatesBackupFilename() {
    Clock fixedClock = Clock.fixed(Instant.parse("2020-07-01T16:02:42.00Z"), ZoneId.of("Europe/London"));
    String absoluteFilename = new DirectoryService(tempDirectory, fixedClock).generateBackupFilename();
    assertTrue(Paths.get(absoluteFilename).getFileName().toString().matches(BACKUPFILE_REGEX),
        format("'%s' does not match regex '%s'", absoluteFilename, BACKUPFILE_REGEX));
    assertThat(absoluteFilename, is(tempDirectory.toAbsolutePath().toString() + "/db-200701-1702.zip"));
  }
}