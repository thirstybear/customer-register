package uk.co.thirstybear.covid.register.model;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;

class CustomerVisitTest {

  private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

  @Test
  public void nameFieldCanOnlyIncludeSafeCharacters() {
    isValid(new CustomerVisit("my name", "01234567890", 1));
    hasValidationViolations(new CustomerVisit("my<name", "01234567890", 1));
    hasValidationViolations(new CustomerVisit("", "01234567890", 1));

    hasValidationViolations(new CustomerVisit("my name", "drop table users;", 1));
    hasValidationViolations(new CustomerVisit("my name", "", 1));

    hasValidationViolations(new CustomerVisit("my name", "01234567890", 0));
    hasValidationViolations(new CustomerVisit("my name", "01234567890", -1));
    hasValidationViolations(new CustomerVisit("my name", "01234567890", 7));
  }

  private void isValid(CustomerVisit customerVisit) {
    assertThat(violationsFor(customerVisit), is(empty()));
  }

  private void hasValidationViolations(CustomerVisit customerVisit) {
    assertThat(violationsFor(customerVisit), is(not(empty())));
  }

  private Set<ConstraintViolation<CustomerVisit>> violationsFor(CustomerVisit customerVisit) {
    return validator.validate(customerVisit);
  }


}