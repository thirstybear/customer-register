package uk.co.thirstybear.covid.register.security;

import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import uk.co.thirstybear.covid.register.db.UserRepository;
import uk.co.thirstybear.covid.register.model.User;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserDetailsServiceImplTest {
  @Test
  public void listAdmins() {
    User user1 = new User("Admin1", "hash1", true);
    User user2 = new User("Admin2", "hash2", true);
    User user3 = new User("Admin3", "hash3", true);

    UserRepository mockUserRepo = mock(UserRepository.class);
    when(mockUserRepo.findAll()).thenReturn(asList(user1, user2, user3));
    LoginAttemptService mockLoginAttemptsService = mock(LoginAttemptService.class);
    when(mockLoginAttemptsService.isBlocked(anyString())).thenReturn(false);
    UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(mockUserRepo, mockLoginAttemptsService);

    assertThat(userDetailsService.adminUsers(),
        containsInAnyOrder(user1.getUsername(), user2.getUsername(), user3.getUsername()));
  }

  @Test
  public void addsAnAdmin() {
    UserRepository mockUserRepo = mock(UserRepository.class);
    LoginAttemptService mockLoginAttemptsService = mock(LoginAttemptService.class);
    when(mockLoginAttemptsService.isBlocked(anyString())).thenReturn(false);
    UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(mockUserRepo, mockLoginAttemptsService);

    assertTrue(userDetailsService.add("myName").isPresent());
  }

  @Test
  public void cannotAddDuplicateAdmin() {
    UserRepository mockUserRepo = mock(UserRepository.class);
    when(mockUserRepo.save(any(User.class))).thenThrow(new DataIntegrityViolationException("Oops!"));

    LoginAttemptService mockLoginAttemptsService = mock(LoginAttemptService.class);
    when(mockLoginAttemptsService.isBlocked(anyString())).thenReturn(false);
    UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(mockUserRepo, mockLoginAttemptsService);

    assertTrue(userDetailsService.add("myName").isEmpty());
  }
}