package uk.co.thirstybear.covid.register.db;


import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AttributeEncryptorTest {
  @Test
  public void encryptionIsReversible() {
    String plaintextString = "mystring";
    AESCipher mockCipher = mock(AESCipher.class);
    when(mockCipher.encrypt(anyString())).thenReturn("1234567890".getBytes());
    when(mockCipher.decrypt("1234567890".getBytes())).thenReturn(plaintextString);
    AttributeEncryptor attributeEncryptor = new AttributeEncryptor(mockCipher);

    String encryptedString = attributeEncryptor.convertToDatabaseColumn(plaintextString);
    assertThat(encryptedString, is(not(plaintextString)));
    assertThat(attributeEncryptor.convertToEntityAttribute(encryptedString), is(plaintextString));
  }
}