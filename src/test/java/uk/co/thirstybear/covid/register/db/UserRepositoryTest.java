package uk.co.thirstybear.covid.register.db;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import uk.co.thirstybear.covid.register.model.User;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@ComponentScan("uk.co.thirstybear.covid.register.db")
@ActiveProfiles("memdb")
class UserRepositoryTest {
  @Autowired
  UserRepository userRepo;

  @Test
  public void writesToDb() {
    User aUser = new User("aUser", "hash", true);
    userRepo.save(aUser);

    assertThat(userRepo.findAll(), contains(aUser));
  }

  @Test
  public void cannotDuplicateNames() {
    User aUser = new User("aUser", "hash", true);
    userRepo.save(aUser);

    assertThrows(DataIntegrityViolationException.class, () -> {
      User aUser2 = new User("aUser", "hash", true);
      userRepo.save(aUser2);
    });
  }

    @Test
    public void cannotHaveNullValues() {
      assertThrows(DataIntegrityViolationException.class, () -> {
        User aUser = new User(null, "hash", true);
        userRepo.save(aUser);
      });
      assertThrows(DataIntegrityViolationException.class, () -> {
        User aUser = new User("myName", null, true);
        userRepo.save(aUser);
      });
    }

}