package uk.co.thirstybear.covid.register.db;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import uk.co.thirstybear.covid.register.model.CustomerVisit;
import uk.co.thirstybear.covid.register.model.CustomerVisitBuilder;

import java.time.LocalDateTime;
import java.util.Collection;

import static java.time.Month.JULY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@ComponentScan("uk.co.thirstybear.covid.register.db")
@ActiveProfiles("memdb")
class CustomerVisitRepositoryTest {
  @Autowired
  CustomerVisitRepository customerRepo;

  @Test
  public void writesToDatabase() {
    LocalDateTime baseTimestamp = LocalDateTime.of(2020, JULY, 7, 11, 5);

    CustomerVisit includedCustomer = aCustomer("Fred Bloggs1", "01234567890", 4, baseTimestamp.plusDays(1));
    customerRepo.save(includedCustomer);
    CustomerVisit includedCustomer3 = aCustomer("Fred Bloggs3", "01234567890", 4, baseTimestamp.minusHours(1L));
    customerRepo.save(includedCustomer3);
    CustomerVisit excludedCustomer = aCustomer("Fred Bloggs4", "01234567890", 4, baseTimestamp.minusDays(1));
    customerRepo.save(excludedCustomer);
    CustomerVisit includedCustomer2 = aCustomer("Fred Bloggs2", "01234567890", 4, baseTimestamp);
    customerRepo.save(includedCustomer2);

    assertThat(customerRepo.findAllByOrderByDateDesc(), contains(includedCustomer, includedCustomer2, includedCustomer3, excludedCustomer));
  }

  @Test
  public void canFindEveryoneSinceDate() {
    LocalDateTime baseTimestamp = LocalDateTime.of(2020, JULY, 7, 11, 5);

    CustomerVisit includedCustomer = aCustomer("Fred Bloggs1", "01234567890", 4, baseTimestamp.plusDays(1));
    customerRepo.save(includedCustomer);
    CustomerVisit includedCustomer3 = aCustomer("Fred Bloggs3", "01234567890", 4, baseTimestamp.minusHours(1L));
    customerRepo.save(includedCustomer3);
    CustomerVisit excludedCustomer = aCustomer("Fred Bloggs4", "01234567890", 4, baseTimestamp.minusDays(1));
    customerRepo.save(excludedCustomer);
    CustomerVisit includedCustomer2 = aCustomer("Fred Bloggs2", "01234567890", 4, baseTimestamp);
    customerRepo.save(includedCustomer2);

    Collection<CustomerVisit> visits = customerRepo.findByDateAfterOrderByDateDesc(baseTimestamp.withHour(0).withMinute(0));
    assertThat(visits.size(), is(3));
    assertThat(visits, contains(includedCustomer, includedCustomer2, includedCustomer3));
  }

  @Test
  public void deleteRecordsBeforeDate() {
    LocalDateTime baseTimestamp = LocalDateTime.of(2020, JULY, 7, 11, 5);

    CustomerVisit includedCustomer = aCustomer("Fred Bloggs1", "01234567890", 4, baseTimestamp.plusDays(1));
    customerRepo.save(includedCustomer);
    CustomerVisit includedCustomer2 = aCustomer("Fred Bloggs2", "01234567890", 4, baseTimestamp);
    customerRepo.save(includedCustomer2);
    CustomerVisit includedCustomer3 = aCustomer("Fred Bloggs3", "01234567890", 4, baseTimestamp.minusDays(21L));
    customerRepo.save(includedCustomer3);
    CustomerVisit excludedCustomer = aCustomer("Fred Bloggs4", "01234567890", 4, baseTimestamp.minusDays(22));
    customerRepo.save(excludedCustomer);

    customerRepo.deleteByDateBefore(baseTimestamp.minusDays(21));
    Iterable<CustomerVisit> visits = customerRepo.findAll();
    assertThat(visits, containsInAnyOrder(includedCustomer, includedCustomer2, includedCustomer3));
  }

  private CustomerVisit aCustomer(String name, String phone, int groupCount, LocalDateTime date) {
    return new CustomerVisitBuilder()
        .withName(name)
        .withPhone(phone)
        .withGroupCount(groupCount)
        .withDate(date)
        .build();
  }

}