create table customervisits
(
    id    bigserial primary key,
    name  varchar(100),
    email varchar(100),
    phone varchar(64),
    date timestamp
);