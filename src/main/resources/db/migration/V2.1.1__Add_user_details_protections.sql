alter table users alter column username set not null;
alter table users alter column password set not null;
alter table users add constraint constraint_uniquename unique(username);
