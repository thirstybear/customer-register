create table users
(
    id    bigserial primary key,
    username  varchar(100),
    password varchar(100),
    enabled boolean
);