package uk.co.thirstybear.covid.register.logging;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public final class Markers {
  public static final Marker SECURITY_EVENT = MarkerFactory.getMarker("SECURITY_EVENT");

  private Markers() {
    // disable construction
  }
}
