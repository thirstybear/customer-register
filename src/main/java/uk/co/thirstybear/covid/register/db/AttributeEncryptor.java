package uk.co.thirstybear.covid.register.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import java.util.Base64;

@Component
public class AttributeEncryptor implements AttributeConverter<String, String> {
  private final AESCipher cipher;

  @Autowired
  public AttributeEncryptor(AESCipher cipher) {
    this.cipher = cipher;
  }

  @Override
  public String convertToDatabaseColumn(String attribute) {
    byte[] encodedData = cipher.encrypt(attribute);
    return Base64.getEncoder().encodeToString(encodedData);
  }

  @Override
  public String convertToEntityAttribute(String dbData) {
    byte[] encodedData = Base64.getDecoder().decode(dbData);
    return cipher.decrypt(encodedData);
  }
}
