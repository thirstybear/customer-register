package uk.co.thirstybear.covid.register.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.time.LocalDateTime.now;
import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;

public class DirectoryService {
  static final String BACKUPFILE_REGEX = "db-\\d{6}-\\d{4}\\.zip";
  private final Path directory;
  private final Clock clock;
  private final DateTimeFormatter filenameGenerator = DateTimeFormatter.ofPattern("'db-'yyMMdd-HHmm'.zip'");

  public DirectoryService(String directory) {
    this(Paths.get(directory));
  }

  DirectoryService(Path directory) {
    this(directory, Clock.systemDefaultZone());
  }

  DirectoryService(Path directory, Clock clock) {
    this.directory = directory;
    this.clock = clock;
  }

  public void cleanBackups() {
    try {
      List<Path> files = Files.list(directory)
          .filter(f -> f.getFileName().toString().matches(BACKUPFILE_REGEX))
          .sorted(comparing(f -> f.getFileName().toString(), reverseOrder()))
          .collect(toList());

      if (files.size() > 5) {
        files.subList(5, files.size()).forEach(FilesWrapper::delete);
      }
    } catch (IOException | FilesWrapper.RuntimeIOException e) {
      e.printStackTrace();
    }
  }

  public String generateBackupFilename() {
    return directory.resolve(filenameGenerator.format(now(clock))).toAbsolutePath().toString();
  }
}

// Hack to get around lambda checked exception. <Sigh>
class FilesWrapper {
  public static void delete(Path path) {
    try {
      Files.delete(path);
    } catch (IOException e) {
      throw new RuntimeIOException(e);
    }
  }

  static class RuntimeIOException extends RuntimeException {
    public RuntimeIOException(IOException e) {
      super(e);
    }
  }
}