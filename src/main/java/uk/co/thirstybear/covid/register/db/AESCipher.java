package uk.co.thirstybear.covid.register.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

@Component
public class AESCipher {
  private static final int ITERATION_COUNT = 1024;
  private static final int KEY_LENGTH = 256;
  private final String passphrase;

  @Autowired
  public AESCipher(@Value("${encryption.secret}") String passphrase) {
    this.passphrase = passphrase;
  }

  public byte[] encrypt(String text) {
    String nullProtectedText = convertNullToEmptyString(text);
    SecureRandom random = new SecureRandom();
    byte[] salt = new byte[16];
    random.nextBytes(salt);

    byte[] ivBytes;
    byte[] encodedText;
    try {
      KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH); // AES-256
      SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
      byte[] key = f.generateSecret(spec).getEncoded();
      SecretKeySpec keySpec = new SecretKeySpec(key, "AES");

      ivBytes = new byte[16];
      random.nextBytes(ivBytes);
      IvParameterSpec iv = new IvParameterSpec(ivBytes);

      Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
      c.init(Cipher.ENCRYPT_MODE, keySpec, iv);
      encodedText = c.doFinal(nullProtectedText.getBytes());
    } catch (GeneralSecurityException e) {
      throw new EncryptionException(e);
    }

    byte[] finalCiphertext = new byte[encodedText.length + 2 * 16];
    System.arraycopy(ivBytes, 0, finalCiphertext, 0, 16);
    System.arraycopy(salt, 0, finalCiphertext, 16, 16);
    System.arraycopy(encodedText, 0, finalCiphertext, 32, encodedText.length);

    return finalCiphertext;
  }

  public String decrypt(byte[] cipherText) {
    if (cipherText.length < 33) throw new EncryptionException("byte[] to decode is too short!");

    final int encodedTextLength = cipherText.length - (2 * 16);
    byte[] ivBytes = new byte[16];
    byte[] salt = new byte[16];
    byte[] encodedText = new byte[encodedTextLength];

    System.arraycopy(cipherText, 0, ivBytes, 0, 16);
    System.arraycopy(cipherText, 16, salt, 0, 16);
    System.arraycopy(cipherText, 32, encodedText, 0, encodedTextLength);

    String decodedText;
    try {
      KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH); // AES-256
      SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
      byte[] key = f.generateSecret(spec).getEncoded();
      SecretKeySpec keySpec = new SecretKeySpec(key, "AES");

      Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
      c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(ivBytes));
      decodedText = new String(c.doFinal(encodedText));
    } catch (GeneralSecurityException e) {
      throw new EncryptionException(e);
    }

    return decodedText;
  }

  private String convertNullToEmptyString(String s) {
    return s == null ? "" : s;
  }

  public static class EncryptionException extends RuntimeException {
    public EncryptionException(GeneralSecurityException e) {
      super(e);
    }

    public EncryptionException(String errorMsg) {
      super(errorMsg);
    }
  }
}
