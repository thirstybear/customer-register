package uk.co.thirstybear.covid.register;

import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import uk.co.thirstybear.covid.register.db.UserRepository;
import uk.co.thirstybear.covid.register.model.User;
import uk.co.thirstybear.covid.register.services.CustomerVisitService;

@Component
public class PostConstructAction {
  private final UserRepository userRepo;
  private final CustomerVisitService customerVisitService;

  @Autowired
  public PostConstructAction(UserRepository userRepo, CustomerVisitService customerVisitService) {
    this.userRepo = userRepo;
    this.customerVisitService = customerVisitService;
  }

  public void initialiseFirstAdmin() {
    if (userRepo.findByUsername("admin") != null)
      return;

    String randomPassword = RandomString.make(14);
    User admin = new User("admin", new BCryptPasswordEncoder().encode(randomPassword), true);
    userRepo.save(admin);

    System.out.println("***********************************************************************");
    System.out.printf("* Initial admin password is %s%n", randomPassword);
    System.out.println("* DO **NOT** LOSE THIS - THIS IS THE ONLY TIME IT WILL BE PRINTED OUT!!");
    System.out.println("***********************************************************************");
  }

  @EventListener
  public void onApplicationEvent(@SuppressWarnings("unused") ContextRefreshedEvent event) {
    initialiseFirstAdmin();
    customerVisitService.expireOldRecords();
  }
}
