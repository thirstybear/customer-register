package uk.co.thirstybear.covid.register.db;

import org.springframework.data.repository.CrudRepository;
import uk.co.thirstybear.covid.register.model.CustomerVisit;

import java.time.LocalDateTime;
import java.util.List;

public interface CustomerVisitRepository extends CrudRepository<CustomerVisit, Long> {
  List<CustomerVisit> findAllByOrderByDateDesc();
  List<CustomerVisit> findByDateAfterOrderByDateDesc(LocalDateTime date);

  void deleteByDateBefore(LocalDateTime date);
}
