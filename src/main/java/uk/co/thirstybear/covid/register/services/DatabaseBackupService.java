package uk.co.thirstybear.covid.register.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class DatabaseBackupService {
  private static final String EVERYDAY_AT0200 = "0 0 02 * * ?";  // HTF to test this?!

  private final JdbcTemplate jdbcTemplate;
  private final DirectoryService directoryService;

  @Autowired
  public DatabaseBackupService(JdbcTemplate jdbcTemplate, @Value("${backupdir}") String backupdir) {
    this.jdbcTemplate = jdbcTemplate;
    this.directoryService = new DirectoryService(backupdir);
  }

  @Scheduled(cron = EVERYDAY_AT0200)
  public void backupDatabase() {
    jdbcTemplate.execute(format("BACKUP TO '%s'", directoryService.generateBackupFilename()));
    directoryService.cleanBackups();
  }
}
