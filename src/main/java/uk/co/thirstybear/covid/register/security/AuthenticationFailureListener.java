package uk.co.thirstybear.covid.register.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static uk.co.thirstybear.covid.register.logging.Markers.SECURITY_EVENT;

@Component
public class AuthenticationFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
  private final LoginAttemptService loginAttemptService;
  private static final Logger logger = LoggerFactory.getLogger(AuthenticationFailureListener.class);

  // Unavoidable Spring yukkyness.
  // This class seems to be only place to shoehorn bruteforce attack protection into Spring Security?
  @Autowired
  private ClientIP clientIP;

  @Autowired
  public AuthenticationFailureListener(LoginAttemptService loginAttemptService) {
    this.loginAttemptService = loginAttemptService;
  }

  @Override
  public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
    Authentication authentication = event.getAuthentication();
    logger.warn(SECURITY_EVENT, "Failed login for User \"{}\" from IP {}", authentication.getName(), clientIP.toString());
    loginAttemptService.loginFailed(clientIP.toString());
  }
}
