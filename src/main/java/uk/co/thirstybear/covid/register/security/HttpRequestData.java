package uk.co.thirstybear.covid.register.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Component
public class HttpRequestData {
  private final HttpServletRequest httpServletRequest;

  public HttpRequestData(HttpServletRequest httpServletRequest) {
    this.httpServletRequest = httpServletRequest;
  }

  public Optional<String> getXForwardFor() {
    return Optional.of(httpServletRequest.getHeader("X-Forwarded-For"));
  }

  public String getRemoteAddr() {
    return httpServletRequest.getRemoteAddr();
  }
}
