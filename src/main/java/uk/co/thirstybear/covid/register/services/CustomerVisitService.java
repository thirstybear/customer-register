package uk.co.thirstybear.covid.register.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import uk.co.thirstybear.covid.register.db.CustomerVisitRepository;
import uk.co.thirstybear.covid.register.model.CustomerVisit;

import javax.transaction.Transactional;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static java.time.LocalDateTime.now;

@Service
public class CustomerVisitService {
  private static final long EXPIRY_PERIOD = 21L;
  private static final String EVERYDAY_AT0600 = "0 0 06 * * ?";  // HTF to test this?!

  private static final Logger logger = LoggerFactory.getLogger(CustomerVisitService.class);
  private final CustomerVisitRepository customerVisitRepo;
  private final Clock clock;

  @Autowired
  public CustomerVisitService(CustomerVisitRepository customerVisitRepo) {
    this(customerVisitRepo, Clock.systemDefaultZone());
  }

  CustomerVisitService(CustomerVisitRepository customerVisitRepo, Clock clock) {
    this.customerVisitRepo = customerVisitRepo;
    this.clock = clock;
  }

  public CustomerVisit logVisit(CustomerVisit customerVisit) {
    return customerVisitRepo.save(customerVisit);
  }

  public List<CustomerVisit> allVisits() {
    return customerVisitRepo.findAllByOrderByDateDesc();
  }

  public List<CustomerVisit> visitsSince(LocalDate date) {
    return customerVisitRepo.findByDateAfterOrderByDateDesc(date.atStartOfDay());
  }

  @Scheduled(cron = EVERYDAY_AT0600)
  @Transactional
  public void expireOldRecords() {
    LocalDateTime expiryDate = now(clock).minusDays(EXPIRY_PERIOD);
    logger.debug("Expiring data older than " + expiryDate);
    customerVisitRepo.deleteByDateBefore(expiryDate);
  }
}
