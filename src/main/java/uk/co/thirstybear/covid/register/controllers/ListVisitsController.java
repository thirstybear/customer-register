package uk.co.thirstybear.covid.register.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uk.co.thirstybear.covid.register.model.CustomerVisit;
import uk.co.thirstybear.covid.register.services.CustomerVisitService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping(value = "/visits")
public class ListVisitsController {
  private final CustomerVisitService customerVisitService;

  @Autowired
  public ListVisitsController(CustomerVisitService customerVisitService) {
    this.customerVisitService = customerVisitService;
  }

  @RequestMapping(method = GET)
  public String listOfRegisteredVisitors(Model model) {
    model.addAttribute("fromdate", LocalDate.now().minusDays(21));
    List<CustomerVisit> visitList = customerVisitService.allVisits();
    model.addAttribute("visits", visitList);
    model.addAttribute("registrations", visitList.size());
    model.addAttribute("visitors", visitList.stream().mapToInt(CustomerVisit::getGroupcount).sum());
    return "listview";
  }

  @RequestMapping(method = POST)
  public String listOfRegisteredVisitors(@RequestParam(name = "since-date") @DateTimeFormat(pattern = "dd MMMM yyyy") LocalDate date, Model model) {
    model.addAttribute("fromdate", date);
    List<CustomerVisit> visitList = customerVisitService.visitsSince(date);
    model.addAttribute("visits", visitList);
    model.addAttribute("registrations", visitList.size());
    model.addAttribute("visitors", visitList.stream().mapToInt(CustomerVisit::getGroupcount).sum());
    return "listview";
  }
}
