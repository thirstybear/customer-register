package uk.co.thirstybear.covid.register.security;

import org.springframework.stereotype.Component;

@Component
public class ClientIP {
  private final HttpRequestData httpRequestData;

  public ClientIP(HttpRequestData httpRequestData) {
    this.httpRequestData = httpRequestData;
  }

  @Override
  public String toString() {
    return httpRequestData.getXForwardFor()
        .orElse(httpRequestData.getRemoteAddr());
  }
}
