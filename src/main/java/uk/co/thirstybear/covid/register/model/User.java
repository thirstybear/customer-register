package uk.co.thirstybear.covid.register.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String username;
  private String password;
  private boolean enabled;

  protected User() {
    // required by JPA
  }

  public User(String username, String hashedPassword, boolean enabled) {
    this.username = username;
    this.password = hashedPassword;
    this.enabled = enabled;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public boolean isEnabled() {
    return enabled;
  }
}
