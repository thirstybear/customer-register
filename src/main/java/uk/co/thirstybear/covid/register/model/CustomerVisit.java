package uk.co.thirstybear.covid.register.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import uk.co.thirstybear.covid.register.db.AttributeEncryptor;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

@Entity
@Table(name = "customervisits")
public class CustomerVisit {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Convert(converter = AttributeEncryptor.class)
  private String name;
  @Convert(converter = AttributeEncryptor.class)
  private String phone;
  private int groupcount;

  private LocalDateTime date;

  protected CustomerVisit() {
    // required by JPA
  }

  @JsonCreator
  public CustomerVisit(String name, String phone, int groupcount) {
    this(name, phone, groupcount, now());
  }

  CustomerVisit(String name, String phone, int groupcount, LocalDateTime date) {
    this.date = date;
    this.name = name;
    this.phone = phone;
    this.groupcount = groupcount;
  }

  @Pattern(regexp = "^[a-zA-Z0-9 .-]+$", message = "Unsafe characters found in the name field")
  public String getName() {
    return name;
  }

  @Pattern(regexp = "^[\\+0][0-9 .-]+$", message = "Unexpected characters found in the phone field")
  public String getPhone() {
    return phone;
  }

  @Min(value = 1, message = "Group size cannot be less than 1")
  @Max(value = 6, message = "Group size cannot be more than 6")
  public int getGroupcount() {
    return groupcount;
  }

  public LocalDateTime getDate() {
    return date;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", phone='" + phone + '\'' +
        ", date=" + date +
        '}';
  }
}
