package uk.co.thirstybear.covid.register.security;

public class AppSecurityException extends RuntimeException {
  public AppSecurityException(String message) {
    super(message);
  }
}
