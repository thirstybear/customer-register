package uk.co.thirstybear.covid.register.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthenticationSuccessListener
    implements ApplicationListener<AuthenticationSuccessEvent> {
  private final LoginAttemptService loginAttemptService;
  private final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessListener.class);

  // Unavoidable Spring yukkyness.
  // This class seems to be only place to shoehorn bruteforce attack protection into Spring Security?
  @Autowired
  private ClientIP clientIP;

  @Autowired
  public AuthenticationSuccessListener(LoginAttemptService loginAttemptService) {
    this.loginAttemptService = loginAttemptService;
  }

  @Override
  public void onApplicationEvent(AuthenticationSuccessEvent event) {
    Authentication authentication = event.getAuthentication();
    logger.info("User \"{}\" logged in from IP {}", authentication.getName(), clientIP.toString());
    loginAttemptService.loginSucceeded(clientIP.toString());
  }
}
