package uk.co.thirstybear.covid.register.security;

import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uk.co.thirstybear.covid.register.db.UserRepository;
import uk.co.thirstybear.covid.register.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Optional;

import static java.util.stream.Collectors.toUnmodifiableList;
import static uk.co.thirstybear.covid.register.logging.Markers.SECURITY_EVENT;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
  private final UserRepository userRepo;
  private final LoginAttemptService loginAttemptsService;
  private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

  // Unavoidable Spring yukkyness.
  // This class seems to be only place to shoehorn bruteforce attack protection into Spring Security?
  @Autowired
  private ClientIP clientIP;

  @Autowired
  public UserDetailsServiceImpl(UserRepository userRepo, LoginAttemptService loginAttemptsService) {
    this.userRepo = userRepo;
    this.loginAttemptsService = loginAttemptsService;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    String ip = getClientIP();
    if (loginAttemptsService.isBlocked(ip)) {
      logger.warn(SECURITY_EVENT, "Blocked login from IP {}: too many failed attempts", ip);
      throw new AppSecurityException(String.format("Login from IP %s is blocked due to multiple failed logins", ip));
    }

    User user = userRepo.findByUsername(username);

    if(user == null)
      throw new UsernameNotFoundException("Username or password was incorrect");

    return new UserDetailsImpl(user);
  }

  public Collection<String> adminUsers() {
    return userRepo.findAll().stream()
        .map(User::getUsername)
        .collect(toUnmodifiableList());
  }

  public Optional<String> add(String name) {
    String password = RandomString.make(14);

    User user = new User(name, new BCryptPasswordEncoder().encode(password), true);

    try {
      userRepo.save(user);
      return Optional.of(password);
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  private String getClientIP() {
    return clientIP.toString();
  }
}
