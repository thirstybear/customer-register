package uk.co.thirstybear.covid.register.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uk.co.thirstybear.covid.register.security.UserDetailsServiceImpl;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
  private final UserDetailsServiceImpl userDetailsService;

  @Autowired
  public AdminController(UserDetailsServiceImpl userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  @RequestMapping(method = GET)
  public String addOrListAdmins(@RequestParam(required = false, name="new") String adminName, Model model) {
    if (adminName == null) {
      model.addAttribute("admins", userDetailsService.adminUsers());
      return "listadmins";
    }

    String generatedPassword = userDetailsService.add(adminName).orElse("FAILED TO CREATE ADMIN. Duplicate?");
    model.addAttribute("name", adminName);
    model.addAttribute("password", generatedPassword);
    return "newadmin";
  }
}
