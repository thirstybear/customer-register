package uk.co.thirstybear.covid.register.security;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class LoginAttemptService {
  static final int MAX_ATTEMPTS = 5;
  private final LoadingCache<String, Integer> attemptsCache;

  public LoginAttemptService() {
    attemptsCache = CacheBuilder.newBuilder().
        expireAfterWrite(1, TimeUnit.DAYS).build(new CacheLoader<String, Integer>() {
      public Integer load(String key) {
        return 0;
      }
    });
  }

  public void loginSucceeded(String key) {
    attemptsCache.invalidate(key);
  }

  public void loginFailed(String key) {
    int attempts = 0;
    try {
      attempts = attemptsCache.get(key);
    } catch (ExecutionException e) {
      // ignore and continue
    }
    attempts++;
    attemptsCache.put(key, attempts);
  }

  public boolean isBlocked(String key) {
    try {
      return attemptsCache.get(key) > MAX_ATTEMPTS;
    } catch (ExecutionException e) {
      return false;
    }
  }
}
