package uk.co.thirstybear.covid.register.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import uk.co.thirstybear.covid.register.model.CustomerVisit;
import uk.co.thirstybear.covid.register.services.CustomerVisitService;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class CustomerVisitController {
  private final CustomerVisitService customerVisitService;
  private final String identifyingString;

  @Autowired
  public CustomerVisitController(CustomerVisitService customerVisitService, @Value("${msg.identifier}") String identifer) {
    this.customerVisitService = customerVisitService;
    identifyingString = identifer;
  }

  @RequestMapping(value="/", method = GET)
  public String index(Model model) {
    model.addAttribute("identity", identifyingString);
    return "index";
  }

  @RequestMapping(value = "/register", method = POST)
  public String registerVisit(@ModelAttribute CustomerVisit customerVisit) {
    customerVisitService.logVisit(customerVisit);
    return "registered";
  }

  @RequestMapping(value = "/faq.html", method = GET)
  public String showFaqPage() {
    return "faq";
  }
}
