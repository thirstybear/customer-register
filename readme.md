# Customer Registration Web App

This is a _simple_ web application to allow customers of, say, a pub, register their
details through a web form.

## Initial Requirements
* Customers register details through a webform. Most likely
    * Name
    * Email
    * Contact number
    * GDPR opt-in?
* Emails are checked for validity
* An email is sent to confirm registration and opt-in/terms
* An "I have registered" page is presented after successful registration
* System stores datestamp of visit and source IP automatically
* All data is stored _encrypted_
* System rejects multiple registrations in a short period (3 hours? Anti-spam, based on IP and email)
* Entries auto-delete 3 weeks after entry
* Can list everyone on record who visited on a certain date, and 14(?) days afterwards
* Send an email to the above if an infection is found?
* Admins can view & CRUD Customer data 
    
## Tech Stack
Currently Spring Boot & MVC etc. 
